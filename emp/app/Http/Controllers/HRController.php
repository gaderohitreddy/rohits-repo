<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\EMP;
use App\Mail\Send_UserId;
class HRController extends Controller
{
    public function dashboard()
    {
    	// return 'somethign woring';
    	return view('hr.dashboard');
    }
    public function create()
    {
    	return view('hr.newemp');
    }
    public function newemp(Request $re)
    {
        // return $re;
        $val  = Validator::make($re->all(),[
            'name' => 'required|min:5',
            'email'=> 'required|email',
            'P_number'=> 'required|numeric',
            'address' => 'required'
        ]);
        if ($val->fails()) {
           return redirect()->route('newemp')->withErrors($val)->withInput();;
        }
        $emp = new EMP();
        $emp->email = $re->email;
        $emp->_token = str_random(40);
        $emp->save();
        
        \Mail::to($re->email)->send(new Send_UserId($emp->id));
        return "E-mail has sent.";

    }
}
