<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EMP extends Model
{
    protected $fillable = ['username','phone','email','address','roll',
    '_token'];
    protected $table = 'emps';
}
