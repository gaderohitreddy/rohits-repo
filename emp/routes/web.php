<?php
Route::get('/', function () {
    return view('welcome');
});
Route::prefix('hr')->group(function(){

	Route::get('dashboard','HRController@dashboard')->name('hr-dashboard');
	Route::post('dashboard','HRController@newemp')->name('post.user');
	Route::get('create','HRController@create')->name('newemp');
	Route::get('assignment','HRController@assignment')->name('assignment');
});
Route::prefix('dev')->group(function(){

	Route::get('dashboard',"EmpController@dashboard")->name('emp-dashboard');

});
