@extends('layouts.app')
@section("title","Dashboard")
@section('style')
	<style type="text/css">

	</style>
@endsection
@section('main')
		
			<div class="row">
			
				<div class="bg col-3 ">
					@yield('left')
				</div>
				<div class="col-9">
					@yield('right')
				</div>
			</div>
		
	

@endsection
@section('js')


@endsection