@extends('hr.dashboard')
@section('right')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form method="post" action="{{ route('post.user') }}">
  @csrf
  <div class="form-group">
    <label for="exampleInputEmail1">Employe Name</label>
    <input type="text" class="form-control" name="name" required aria-describedby="emailHelp" placeholder="Name" required value=" {{ old('email')}}">
    <small name="emailHelp" class="form-text text-muted">Enter Full Name</small>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Email</label>
    <input type="email" class="form-control" name="email" placeholder="email" required value=" {{ old('email')}}">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Contact Number</label>
    <input type="text" class="form-control" name="P_number" placeholder="Contact Number" required value=" {{ old('email')}}">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Address</label>
    <textarea  class="form-control" name="address" placeholder="Address" required></textarea value=" {{ old('email')}}">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection