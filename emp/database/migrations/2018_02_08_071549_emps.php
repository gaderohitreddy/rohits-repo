<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Emps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emps',function(Blueprint $table){

            $table->increments('id');
            $table->string('email')->default('NULL');
            $table->string('username')->default('NULL');
            $table->string('password')->default('NULL');
            $table->integer('phone')->default('0');
            $table->string('address')->default('NULL');
            $table->integer('roll')->default('0');
            $table->string('_token')->default('NULL');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    public function down()
    {
        Schema::drop('emps');
    }
}
