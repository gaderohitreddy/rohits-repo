<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password','thumburl',"token"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
     protected function posts()
     {       
         return $this->hasOne('App\Post');  
     }
    //  public function followers()
    // {
    //     return $this->belongsToMany(User::class, 'followers', 'leader_id', 'follower_id')->withTimestamps();
    // }

    // public function followings()
    // {
    //     return $this->belongsToMany(User::class, 'followers', 'follower_id', 'leader_id')->withTimestamps();
    // }
}
