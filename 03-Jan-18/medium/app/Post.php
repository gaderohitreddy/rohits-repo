<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Post extends Model
{
 use SoftDeletes;
	protected $table = 'posts';
    protected $fillable = ['auth_id','title','body','upi','tags'];
    protected $data = [	'deleted_at'];
}
