<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Post;
use Validator;
use Image;

class ProfileController extends Controller
{
    public function index(Request $req)
    {
    	$user = Auth::user();
    	return view('profile',compact('user'));
    }
    public function upload(Request $req)
    {
    	$val = Validator::make($req->all(),[
    		'image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048']);
    	if ($val->fails()) {
    		return redirect()->route('post')->withErrors($val)->withInput();
    	}else{
    	// return $req->file('image');
    	// if($req->file("image")){

    		$image = $req->file("image");
    		$imagename = Auth::user()->username.'.'.$image->getClientOriginalExtension();
    		$thumb_img = Image::make($image->getRealPath())->resize(50,50);

    		$destinationPath = public_path('uploads/normal_images');
    		Auth::user()->thumburl = Auth::user()->username.'.'.$image->getClientOriginalExtension();
            Auth::user()->save();
        	$image->move($destinationPath, $imagename);
    		$thumb_img->save($destinationPath.'/thumbimg/'.$imagename,80);

    		 return back()
            ->with('success','Image Upload successful')
            ->with('imagename',$imagename);
		}
    	// }
    }
}
