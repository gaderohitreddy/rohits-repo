<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use App\User;
use App\Post;
use Validator;

use App\Mail\OrderShipped;

class PostController extends Controller
{
    public function index()
    {
    	

      return view('dashboard');
    }
    public function save(Request $req)
    {
    	$val  =  Validator::make($req->all(),[
    			'title'=>'required|unique:posts',
    			'body' =>'required'
    	]);
    	if ($val->fails()) {
    		return redirect()->route('post')->withErrors($val)->withInput();
    	}else{
    		$post = new Post();
    		$post->title = $req->title;
    		$post->body = $req->body;
    		$post->tags = '#post';
    		$post->upi  = str_random(20);
    		$post->usr_id = Auth::id();	
    		if ($post->save()) {
    			flash('<center> Your Post is saved..</center>')->success();
    			return redirect()->route('welcome');
    		}
    	}
    }
    public function list()
    {
        $posts = Post::where('usr_id',Auth::id())->get(['upi','title']);
        // return $posts;
        return view('list')->with('posts',$posts);
    }
    public function view(Request $req)
    {
        
         $post = Post::where('upi',$req->id)->first();
         // return $post;
         return view('view')->with('post',$post);
         // return $post;
    }
    public function getedit(Request $req)
    {
         $post = Post::where('upi',$req->id)->first();
         return view('editpost')->with('post',$post);
    }
    public function savechages(Request $req)
    {
        $val  =  Validator::make($req->all(),[
                'title'=>'required',
                'body' =>'required'
        ]);
        if ($val->fails()) {
            return redirect()->route('getedit',$req->id)->withErrors($val)->withInput();
        }else{
              $post = Post::where('upi',$req->id)->first();
              $post->title = $req->title;
              $post->body  = $req->body;
              if ($post->save()) {
                  flash('<center> Your Post is Updated..</center>')->success();
                return redirect()->route('view',$req->id);
              }
              else{
                return "didn't save in db";
              }
            }

    }
    public function delete(Request $req)
    {
      $post = Post::where('upi',$req->id)->first();
      $post->delete();
      flash('<center> Your Post is Updated..</center>')->error();
                return redirect()->route('myposts');  
    }
    public function allposts(Request $req)
    {
      $users = Post::withTrashed()->where('usr_id', 1)->get();
      return $users;
    }
    public function explore(Request $req)
    {
      $posts = Post::inRandomOrder()->take(15)->get(); 
      return view( 'explore')->with('posts',$posts);
    }
    public function showuser(Request $req)
    {
      $user = User::where('username',$req->username)->first();
      if (count($user)) {
         $posts = Post::where('usr_id',$user->id)->get(['id','title','upi',"usr_id"]);
          return view('userlist',['posts' => $posts,'user' => $user]);
      }
      return'no user exit with name';
    }
    public function follow(Request $req)
    {
      return $req->username;
      $user = User::where('usename',$req->username)->first();
      if(! $user) {
        
         return redirect()->back()->with('error', 'User does not exist.'); 
     }

    $user->followers()->attach(auth()->user()->id);
    return redirect()->back()->with('success', 'Successfully followed the user.');
    }
    public function unfollow(Request $req)
    {
      $user = User::where('username',$req->username)->first();
      if(! $user) {
        
         return redirect()->back()->with('error', 'User does not exist.'); 
     }
    $user->followers()->detach(auth()->user()->id);
    return redirect()->back()->with('success', 'Successfully unfollowed the user.');
    }
    public function sendmail()
    {
      Mail::to(Auth::user())->send(new OrderShipped());
      return "mail have be send.. check you mail ";
    }
}
