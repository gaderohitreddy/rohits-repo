<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\User ;

use Validator;

class UserController extends Controller
{
    public function login(Request $request)
    {
    	$val = Validator::make($request->all(),[
    		'email'    =>'required',
    		'password' =>'required'
    		]);
    	if ($val->fails()) {
    		return redirect()->route('user')->with('login',true)->withErrors($val)->withInput();
    	}
        if(filter_var($request->email,FILTER_VALIDATE_EMAIL))
        {
            if (Auth::attempt(['email'=>$request->email,'password'=>$request->password]))
            {
                $username = Auth::user()->username;
                    flash("<center>Welcome $username...</center>");
                    return redirect()->route('welcome')->with('s',true);
                
            }
        }
    	else if (Auth::attempt(['username'=>$request->email,'password'=>$request->password]))
    	{
                $name = Auth::User()->username;
    			flash("<center>Welcome $name ...</center>");
    			return redirect()->route('welcome')->with('s',true);
    	}
    	
   		flash("<center>invalid username or password</center>")->error();
   		return redirect()->route('user');

    }
    public function create(Request $request)
    {
    	$val = Validator::make($request->all(),[
    			'username'	=> 'required',
    			'email' 	=> 'required|email|unique:users',
    			'password'	=> 'required|min:6',
    			'password_again' => 'required|same:password',
                
                    	]);	
    	if ($val->fails()) {
    		return redirect()->route('user')->withErrors($val)->withInput();
    	}
    	else
    	{
    		$user = new User();
    		$user->username = $request->username;
    		$user->email = $request->email;
    		$user->password = Hash::make($request->password);
            $user->thumburl = 'default.jpeg';
    		if($user->save()){
    					
    				flash('center>User is Created Sucessfully now you can login</center>')->success();
    			return redirect()->route('user');
    		}

    	}
    }
}
