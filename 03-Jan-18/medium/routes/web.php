<?php

Route::get('/','StaticController@welcome')->name("welcome");

Route::group(['middleware'=>'guest'],function(){
	Route::get('/login','StaticController@user')->name('user');
	Route::post('/login','UserController@login')->name('login');
	Route::post('/create','UserController@create')->name('newuser');
});

Route::get('view/{id}','PostController@view')->name('view');
Route::get('/explore',"PostController@explore")->name('explore');
Route::get('/@{username}',"PostController@showuser")->name('showuser');
// auth routes
Route::middleware(['auth'])->group(function(){

	Route::get('/dashboard','PostController@index')->name('dashboard');
	
	Route::get('/newpost',function(){return view('post');})->name('newpost');
	Route::post('/newpost','PostController@save')->name('post');


	Route::get('/myposts','PostController@list')->name('myposts');

	Route::get('edit/{id}',"PostController@getedit")->name('getedit');
	Route::post('/savechages/{id}',"PostController@savechages")->name('savechages');

	Route::get('delete/{id}',"PostController@delete")->name('getdelete');

	Route::get('/@{username}&follow=true',"PostController@follow")->name('follow');
	Route::get('/@{username}&follow=false',"PostController@unfollow")->name('unfollow');
	Route::get('/profile','ProfileController@index')->name('profile');
	Route::post('/profile/upload','ProfileController@upload')->name('upload');
	Route::get('/logout',function ()
	{
		Auth::logout();
		flash('<center>logged out...!</center>')->success();
		return Redirect::route('welcome');
	})->name('logout');



	// Testing email service

	Route::get('/sendmail','PostController@sendmail')->name("sendmail");

});
// raw data 
Route::get('/db_posts','PostController@allposts');