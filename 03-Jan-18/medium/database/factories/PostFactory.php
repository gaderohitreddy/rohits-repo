<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->realText($maxNbChars = 100, $indexSize = 2),
        'body'  => $faker->realText($maxNbChars = 900, $indexSize = 2),
        'upi' => str_random(20),
        'tags' => '#posts',
        'usr_id' => App\User::all()->random()->id,
         'created_at' => $faker->dateTimeBetween('-3 years', 'now')

    ];
});
