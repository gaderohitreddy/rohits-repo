<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Posts extends Migration
{
    public function up()
    {
        Schema::create('posts',function(Blueprint $tb)
            {
                $tb->increments('id');
                $tb->text(('title'));
                $tb->mediumText('body');
                $tb->string('upi');
                 $tb->softDeletes();
                $tb->string('tags');
                $tb->timestamps();
                $tb->integer('usr_id')->unsigned();
            });
        Schema::table('posts',function (Blueprint $tb)
        {
                $tb->foreign('usr_id')->references('id')->on('users');
        });
    }

    public function down()
    {
            Schema::dropIfExists('posts');
    }
}
