@extends('layouts.app')
@section('title','Profile')
@section('style')
	<style type="text/css">
		.centering{
			margin:0 auto;
			float:none;
		}
	</style>
@endsection
@section('content')
	<center>
		<div class="row">
			<div class="col-lg-5 centering">
				<h3>Update Profile pic</h3>
				<hr>
				<form action="{{ route('upload') }}" method="post" accept-charset="UTF-8" enctype="multipart/form-data">

			{{ csrf_field() }}
			<div >
				<label>Profiel Pic</label>
			 {!! Form::file('image', array('class' => 'image')) !!}
			</div>
			<button class="btn btn-primary" type="submit">Upload</button>
			{{-- Upload Profile Pic:<input type="file" name="image"> --}}
		</form>	
			</div>
		</div>
	</center>
	
@endsection