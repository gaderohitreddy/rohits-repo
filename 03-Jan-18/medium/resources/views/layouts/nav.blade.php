<nav class="nav navbar-default ">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      	@auth
      		@php
      		$user = Auth::user();
      		$string = "/uploads/normal_images/thumbimg/".Auth::user()->thumburl;
      			$url = asset($string)
      			 ;
      		@endphp
    		  <a class="navbar-brand" href="{{ route('showuser',$user->username) }}"><b>
      			{{$user->username }}      					

      		<img  class="navprofile" src="{{ $url }} ">
      		<li></a></li>

		@else


		@endauth


      </b></a>
		</div>
		  <ul class="nav navbar-nav navbar-right">

		  	<li><a href="{{ route('explore') }}">Explore</a></li>
		  	@auth
		  	<li><a href="{{ route('showuser',$user->username) }}">My Posts</a></li>
		  	<li><a href=" {{ route('profile') }} ">Profile</a></li>
			
			<li><a href="{{ route('newpost') }}"	>New </a></li>

			<li><a href="{{ URL::route('logout') }}">Sign Out</a></li>
		  	@else
		  		<li><a href="{{route('user')}}">Login</a></li>
		  		<li><a href="{{route('user')}}">New User</a></li>
		  		
		  	@endauth
		  </ul>
	</div>
</nav>