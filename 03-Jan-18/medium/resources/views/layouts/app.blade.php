<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="/css/app.css">
	<style type="text/css">
		html, body {
                color: #636b60;
                font-family: 'Raleway', sans-serif;
                font-size: 17px;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
            .navprofile{
            	width: 50px;
            	height: 50px;
                margin:0px;
            	border-radius: 50%;
            }
	</style>
</head>
<body>
	@include('layouts.nav')
	@include('flash::message')
	@yield('content')
</body>
@yield('style')
<script type="text/javascript" src="/js/jqy.min.js"></script>
<script type="text/javascript" src="/js/app.js"></script>
@yield('js')
</html>