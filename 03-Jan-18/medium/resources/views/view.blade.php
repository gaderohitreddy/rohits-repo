@extends('layouts.app')
@section('title','Artical')
@section('style')
	<style type="text/css">
		.title{
			font-style:bold;
			font-weight: 100px;
			font-size: 30px;

		}
		.col-centered{
			float: none;
			margin: 0 auto;
		}
		.user-img{
			width: 50px;
			height: 50px;
			/*background-color: #000;*/
			border-width: 0px;
			border-style: : hidden;
			border-radius: 50%;
			margin: none;
		}
		.author{
			/*margin-top: 15px;*/
			margin-left: 10px; 
		}.time


	</style>
@endsection
@php
	$user  = App\User::find($post->usr_id);
@endphp
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-lg-8 col-centered"><br>
					<div class="row">
						<div class="col-sm-1">
							@php
								$string = "/uploads/normal_images/thumbimg/".$user->thumburl;
							@endphp
							<img class="user-img" src="{{ asset($string) }}" >
						</div>
						<div class="col-md-7">
							<a href="{{ route('showuser',$user->username) }}">
								<i class="author">By {{ $user->username }} <br>	
							
								</i></a>
								<i>&nbsp;{{ $post->updated_at->diffForHumans() }}</span></i>
							
						</div>
					</div>
				<h2 class="title">{{ $post->title}}</h2>

			@auth
				@if ($post->usr_id == Auth::id())
					<a href="{{ route('getedit',$post->upi)}}">#Edit</a>
				@endif
				@endauth
			<hr>
			{{ $post->body }}
			</div>
					
		</div>		
		</center>
	</div>
@endsection