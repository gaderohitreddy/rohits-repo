
@extends('layouts.app')
@section('title','mylist')
@section('style')
	<style type="text/css">
		.col-center{
			margin: 0 auto;
			float: none;
		}
	</style>
@endsection
@section('content')
	<div class="content">
	<div class="row">
		<div class="col-md-9 col-center"> 
		<ul>
			<h2 class="text-center">@ Some posts</h2>
			@foreach ($posts as $post)

					*<a href="view/{{ $post->upi }}">{{ $post->title }}</a>
					@php
						$user = App\User::find($post->usr_id);
					@endphp

					<small class="right">&nbsp;&nbsp;&nbsp;&nbsp;by&nbsp;{{ $user->username }}</small>
					<br>

			@endforeach
		</ul>
		</div>
	</div>
		
			
		</center>
	</div>
@endsection