@extends('layouts.app')
@section("title",'Users')
@section('style')
    <style type="text/css">
        #bar
        {
            width: 5px;
            height: 100%;
            background-color: #000;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="row">
                         @if ($errors->any())
                            <center>
                                <br>
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            <center>
                         @endif
            <div class="col-md-6">
                <div class="col-md-8">
                 <h2>Login</h2>
               
                    <form action="{{route('login')}}" method="post"  id="login">
                        {{csrf_field()}}
                        <div class="form-group">
                            <br>
                            <label>Username / Email</label>
                            <input type="text" name="email" class="form-control" placeholder="Username Please!" value=" {{ old('email')}}">
                            <br>
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Don't forget Password">
                            <br><button id="bt" class="btn btn-primary">Submit</button>
                        </div>
                </form>
                </div>
            </div>
            <div class="col-md-6">
                <div class="col-md-9">
                    <h2>New User</h2>
                 <form action="{{route('newuser')}}" method="post" id="register">
                    {{ csrf_field() }}
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" name="username" class="form-control" placeholder="Username" value=" {{ old('username')}}"><br>

                            <label>Email</label>
                            <input type="text" name="email" class="form-control" placeholder="Emali Please!" value=" {{ old('email')}}"><br>
                            <label>Password</label>
                            <input type="password" name="password" class="form-control" placeholder="Don't forget Password" ><br>
                            <label>Password</label>
                            <input type="password" name="password_again" class="form-control" placeholder="Password"><br>
                            <input type="submit" name="Submit" class="btn btn-primary" value="submit"></button>
                        </div>
                </form>
                </div>
            </div>
        </div>        
    </div>

@endsection
@section('js')

<script type="text/javascript">
    // $("#bt").click(function(event) {
    //     var username = $('[name="email"]').val();
    //     var Password = $('[name="password"]').val();

    //     var user_exp= /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
    //     var user_pas=/^[a-zA-Z ]{2,30}$/;
        
    //     if ((username == "")){
    //         console.log('username is empty');

    //     }
    //     if((Password == "") )
    //     {
    //         console.log('password is empty');
    //         return;
    //     } 
    //     if(!user_exp.test(username)){
    //         console.log('invalid email id');
    //     }
    //     if(!user_pas.test(Password)){
    //         console.log('invalid password');
    //     }

        // $("#login").submit(function () {
        //     console.log('submited');
        // });
    //});
</script>

@endsection