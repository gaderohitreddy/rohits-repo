@extends('layouts.app')
@section('title','Create Post')
@section('style')
		<style type="text/css">
			textarea{
				margin-top: 8px;
				width: 750px;
				height: 460px;
				border-radius: 5px;
				border-color:#ccd0d2 ;
			}
			.centering{
				margin:0 auto;
				float: none;
			}
			form{
				height:800px;
			}
		</style>
@endsection
@section('content')	
<div class="container">
	<div class="row ">
		@if ($errors->any())
                            <center>
                                <br>
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            <center>
                         @endif
		<div class="col-lg-8 centering">
			<form  action="{{ route('post') }}" method="post">
				<h2 align="center">New Post</h2>
		<div class="form-group">
		<input  class="form-control" type="text" name="title" placeholder="Title goes here.................">
		<textarea  type="text" name="body"></textarea>
		<button class="btn btn-success " type="submit" name="submit">Create</button>
		</div>
		{{ csrf_field() }}
	</form>
		</div>
	</form>
		</div>
	</div>
</div>

@endsection