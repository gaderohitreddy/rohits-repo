@extends('layouts.app')
@section('title','Create Post')
@section('style')
		<style type="text/css">
			textarea{
				margin-top: 8px;
				width: 457px;
				height: 460px;
				border-radius: 5px;
				border-color:#ccd0d2 ;
			}
			#for{
				align-content: center;
				text-align: center;
				margin-left: 30%;
			}
		</style>
@endsection
@section('content')	
<div class="container">
	<div class="row" id="for">
		@if ($errors->any())
                            <center>
                                <br>
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            <center>
                         @endif
		<div class="col-md-5 ">
			<form  action="{{ route('savechages',$post->upi) }}" method="post">
				<h2>Edit Post</h2>
		<div class="form-group">
		<input  class="form-control" type="text" name="title" value="{{ $post->title }}">
		<textarea  type="text" name="body" >{{ $post->body }}</textarea>
		</div>
		<button class="btn btn-default" type="submit" name="submit">Update</button>
		{{ csrf_field() }}
	</form>
		</div>
	</form>
		</div>
	</div>
</div>

@endsection