<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Posts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs',function( Blueprint $tb)
            {
                $tb->increments('id');
                $tb->integer('authr_id')->unsigned();
                $tb->string('title');
                $tb->mediumText('dec');
                $tb->string('upi');
                $tb->timestamps();
            });
        Schema::table('blogs',function(Blueprint $tb){
             $tb->foreign('authr_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blogs');
    }
}
