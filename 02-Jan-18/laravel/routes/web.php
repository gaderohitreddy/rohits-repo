<?php
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/app', 'HomeController@index')->name('home');
Route::get('/new','BlogController@main')->name('blog')->middleware('auth');
Route::post('/new',"BlogController@createpost")->name('post');
Route::get('/read',"BlogController@Allposts")->name('read');
Route::get('/view/{upi}/{authr_id}',"BlogController@fullview")->name('fullview.upi.authr_id');
