@extends('layouts.app')
@section('content')
	<div class="container" >
		<center><h1>New Post</h1></center>
		 @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
              @foreach ($errors->all() as $error)
                  <li>*  {{ $error }}</li>
              @endforeach
          </ul>
      </div><br />
      @endif
      @if (\Session::has('success'))
      <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p>
      </div><br />
      @endif
		<form action="{{route('post')}}" method="post">
			<div class="form-group" style="max-width: 700px; margin: auto;">
			{{ csrf_field() }}
			<h2>Title</h2>
			<input type="text" name="Title" class="form-control" placeholder="Another awsome post.....">
			<br><br>

			<textarea type="text" name="dec" class="form-control" rows="15" placeholder="Express your ideas in a beautiful and constuctive way here . . . . . . . . . . . . . . . . . . . . . . !"></textarea>
			<br>
				<button class="btn btn-primary " type="submit" > Submit</button>
			<br><br>
		</div>
		</form>
	</div>
@endsection