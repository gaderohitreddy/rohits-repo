@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <ul class="list-group">
                        
                    @foreach($posts as $post)
                        <h2 class="list-group-item-heading">
                            <a href="{{route('fullview.upi.authr_id',['upi'=>$post->upi,"authr"=>$post->authr_id])}}">|{{$post->title}}


                            </a>
                        </h2><br>
                    @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
