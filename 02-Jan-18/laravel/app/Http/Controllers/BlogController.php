<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Blog;

class BlogController extends Controller
{
    public function main()
    {
    	return view('newpost');
    }
    public function createpost(Request $data)
    {
		$post = $this->validate(request(),[
				'Title' => 'required',
				'dec'=>'required'
			]);    	
		$newpost =["title" => $post['Title'],"dec"=>$post['dec'],"authr_id"=>Auth::id(),'upi'=>str_random(26)];
		Blog::create($newpost);
		return back()->with('success','The Post is successfully created');
    }
    public function Allposts()
    {
    	$posts = DB::table('blogs')->get();
    	return view('all')->with('posts',$posts);
    }
    public function fullview($upi,$authr_id)
    {
    	$post = DB::table('blogs')->where('upi',$upi)->where('authr_id',$authr_id)->get()->first();
    	$author = DB::table('users')->where('id',$authr_id)->first();
    	return view('post')->with('post',$post)->with('author',$author);
    }
}
